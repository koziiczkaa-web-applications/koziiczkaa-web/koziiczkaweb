import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {FooterSection} from '../../src/sections/FooterSection';

describe('FooterSection', (): void => {
	it('renders', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<FooterSection/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});