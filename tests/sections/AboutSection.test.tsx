import '../__mocks__/MockI18n';
import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {AboutSection} from '../../src/sections/AboutSection';

describe('AboutSection', (): void => {
	it('renders', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<AboutSection/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
