import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {SingleProject} from '../../src/components/SingleProject';
import {projects} from '../../src/DataProviders';

describe('SingleProject', (): void => {
	it('renders', (): void  => {
		//when
		const wrapper: ShallowWrapper = shallow(<SingleProject project={projects[0]}/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
