import {shallow, ShallowWrapper} from 'enzyme';
import React from 'react';
import {Timeline} from '../../src/components/Timeline';
import {timelineWorkElements} from '../../src/DataProviders';

describe('Timeline', (): void => {
	it('renders', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<Timeline elements={[]} title='test'/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with elements', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<Timeline elements={timelineWorkElements} title='test'/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with renderName', (): void => {
		//when
		const wrapper: ShallowWrapper = shallow(<Timeline elements={timelineWorkElements} title='test' renderName/>);

		//then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles element change', (): void => {
		//given
		const wrapper: ShallowWrapper = shallow(<Timeline elements={timelineWorkElements} title='test'/>);

		//when
		wrapper.find('#timeline-line_element-0').simulate('click');

		//then
		expect(wrapper).toMatchSnapshot();
	});
});
