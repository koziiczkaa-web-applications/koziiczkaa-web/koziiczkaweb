import {TFunction} from 'i18next';
import React from 'react';

jest.mock('react-i18next', (): {} => ({
	useTranslation: (): { t: (key: string) => string } => ({t: (key: string): string => key}),
	Trans: (props: { i18nKey: string }): React.ReactNode => props.i18nKey
}));

jest.mock('i18next', (): object => ({
	use: (): object => ({
		init: (): Promise<TFunction> => Promise.resolve((key: string): string => key)
	}),
	changeLanguage: (lang: string): Promise<string> => Promise.resolve(lang)
}));
