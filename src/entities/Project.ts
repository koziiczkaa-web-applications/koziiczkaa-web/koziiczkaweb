import {Link} from './Link';

export interface Project {
	readonly id: number;
	readonly title: string;
	readonly description: string;
	readonly technologies: string[];
	readonly links: Link[];
}
