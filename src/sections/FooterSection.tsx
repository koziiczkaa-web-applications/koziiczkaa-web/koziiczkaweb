import React from 'react';
import {SectionTitle} from '../components/SectionTitle';
import './FooterSection.scss';
import {useTranslation} from 'react-i18next';
import {SocialMediaTile} from '../components/SocialMediaTile';
import LinkedIn from '../../resources/images/linkedin.svg';
import Discord from '../../resources/images/discord.svg';
import Facebook from '../../resources/images/facebook.svg';
import GMail from '../../resources/images/gmail.svg';

export function FooterSection(): JSX.Element {
	const {t} = useTranslation();

	return <footer>
		<div className='footer'>
			<div className='footer-left'>
				<SectionTitle title='Contact' zIndex={100}/>
				<div className='footer-left_form'>
					<label htmlFor='contact-email' className='footer-left_form-label'>E-Mail</label>
					<input id='contact-email' type='email' className='footer-left_form-input'/>
					<label htmlFor='contact-subject' className='footer-left_form-label'>{t('footer-subject')}</label>
					<input id='contact-subject' type='text' className='footer-left_form-input'/>
					<label htmlFor='contact-textarea' className='footer-left_form-label'>{t('footer-message')}</label>
					<textarea id='contact-textarea' className='footer-left_form-input footer-left_form-input_textarea'/>
				</div>
			</div>
			<h4 className='footer-divider'>{t('footer-divider')}</h4>
			<div className='footer-right'>
				<h3 className='footer-right_title'>{t('footer-title')}</h3>
				<p className='footer-right_desc'>{t('footer-description')}</p>
				<div className='footer-right_socialmedia'>
					<SocialMediaTile link='/alicja-kozik' icon={<LinkedIn/>}/>
					<SocialMediaTile link='koziiczkaa#2502' icon={<Discord/>}/>
					<SocialMediaTile link='/alicja.kozik.19' icon={<Facebook/>}/>
					<SocialMediaTile link='alicja.kozik00@gmail.com' icon={<GMail/>}/>
				</div>
			</div>
		</div>
		<div className='footer-copyright'>
			<p className='footer-copyright_text'>Designed and coded by Alicja Kozik &copy; 2022 All rights reserved</p>
		</div>
	</footer>;
}
