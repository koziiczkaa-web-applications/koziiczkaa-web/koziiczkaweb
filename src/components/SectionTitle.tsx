import React from 'react';
import './SectionTitle.scss';

export interface SectionTitleProps {
	title: string;
	zIndex?: number;
}

export function SectionTitle(props: SectionTitleProps): JSX.Element {
	return <div className='section-title'>
		<h1 className='section-title--name' style={{zIndex: props.zIndex}}>{props.title}</h1>
	</div>;
}
