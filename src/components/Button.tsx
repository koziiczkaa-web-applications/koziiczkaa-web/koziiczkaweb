import React from 'react';
import './Button.scss';

export interface ButtonProps {
	name: string;
	width?: string;
}

export function Button(props: ButtonProps): JSX.Element {
	return <button className='button' style={{width: props.width}}>
		{props.name}
		<span/><span/><span/><span/>
	</button>;
}
