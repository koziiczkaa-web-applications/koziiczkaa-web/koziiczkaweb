import React from 'react';
import './HamburgerButton.scss';

export interface HamburgerButtonProps {
	onClick: () => void;
	opened: boolean;
	id?: string;
}

export function HamburgerButton(props: HamburgerButtonProps): JSX.Element {
	return <div className='hamburger__wrapper' id={props.id}>
		<button
			className={'hamburger' + (props.opened ? ' hamburger--active' : '')}
			id='hamburgerbtn'
			onClick={props.onClick}
		>
			<span className='hamburger__box'>
				<span className='hamburger__inner'/>
			</span>
		</button>
	</div>;
}
